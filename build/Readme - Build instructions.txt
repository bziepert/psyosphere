Testing package

"Run slowtest.R" runs devtools::check() and devtools::testthat(). However, not
all tests can be properly executed this way. For instance the google maps api
connection goes often wrong with this test. Therefore, there is also a
"slowtest" folder in the "build" directory. "Run slowtest.R" will also execute
the slow tests. The slow tests will ask the user to confirm the correctness of
the result after each test.

A final test should be done by submitting the package to winbuilder
https://win-builder.r-project.org/upload.aspx and upload the source package to
be checked with R-devel.

The slow test is part of the git but will be excluded from package.

"Rebuild data.R" will recreate all data that is stored in the "data" directory
of the package. To run "Rebuild data.R" is usefull if for instance a dependencie
package changes it's data format.
